# Copyright (C) 2024 slata 1.0 Team 
#
# Licensed under the BSD-2-license:
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# https://gitlab.com/slata2/slata-1.0/-/blob/main/LICENSE.md
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__author__ = "slata 1.0 Team"
__copyright__ = "Copyright (C) 2024 slata 1.0 Team - All Rights Reserved"
__license__ = "BSD-2-license"


import os
import subprocess
import re
import xml.etree.ElementTree as ET
import csv
import argparse
import sys

parser = argparse.ArgumentParser(description="This script will allow you to perform encoding, decoding, and extract various metrics including Quality metrics such as PSNR,SSIM,VMAF values (harmonic mean) using SVT-AV1 Encoder, FFmpeg and the user's intended crfs and presets values on y4m videos. Please note that for all paths in this script it is assumed that all files are already in your user directory example: /home/mike_1234")


# Adding arguments for video processing configurations
parser.add_argument('--presets', nargs='+', type=int, default=[2,7], help='List of encoding presets, example: 4 7 Default values are [2,7]' )
parser.add_argument('--crfs', nargs='+', type=int, default=[32,37,42,47,52,57], help='List of CRF values, example: 20 30 Default values are [32,37]')
parser.add_argument('--fps', type=int, default=25, help='Frames per second for the video processing, default is 25')
parser.add_argument('--resolution', type=str, default='1920x1080', help='Resolution for the video processing, default is 1920x1080')


# Adding arguments for paths
parser.add_argument('--enc_path', type=str, default='SVT-AV1/Build/ffmpeg/ffmpeg', help='You need to specify the path for the FFmpeg executable for encoding from this directory, so it is appended to the existing directory path. Otherwise this default path will be appended: SVT-AV1/Build/ffmpeg/ffmpeg')
parser.add_argument('--dec_path', type=str, default='SVT-AV1/Build/ffmpeg/ffmpeg_s', help='You need to specify the path for the FFmpeg executable for decoding from this directory, so it is appended to the existing directory path. Otherwise this default path will be appended: SVT-AV1/Build/ffmpeg/ffmpeg_s')
parser.add_argument('--original_dir', type=str, required=True, help='You need to specify the directory containing the original videos. It is required to proceed with encoding/decoding.')
parser.add_argument('--encoded_dir', type=str, default='encoded-video', help='You need to specify the output directory name for encoded videos, so it is appended to the existing directory path. Default name is encoded-video')
parser.add_argument('--decoded_dir', type=str, default='decoded-video', help='You need to specify the output directory name for decoded videos and metrics, so it is appended to the existing directory path. Default name is decoded-video')
parser.add_argument('--output_file', type=str, default='output_data.tsv', help='You need to specify the output file path for saving the data from this directory, so it is appended to the existing directory path. Default path appended is output_data.tsv')



# Function to run a command and print its output
def run_command(command):
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    print(result.stdout.decode())
    print(result.stderr.decode())
    # handles errors related to the execution of the command
    if result.returncode != 0:
        print(f"Error: The command '{command}' failed with exit code {result.returncode}")
        raise RuntimeError("Command execution failed.")


# Function to encode videos
def encode_videos(videos, enc_ffmpeg_path, original_videos_directory, encoded_directory, presets, crfs):
    os.makedirs(encoded_directory, exist_ok=True)
    for video in videos:
        video_name = os.path.splitext(video)[0]
        original_video_path = os.path.join(original_videos_directory, video)
        for preset in presets:
            for crf in crfs:
                output_bin_path = os.path.join(os.path.abspath(encoded_directory), f"{video_name}_M{preset}_CRF{crf}.bin")
                output_txt_path = os.path.join(os.path.abspath(encoded_directory), f"{video_name}_M{preset}_CRF{crf}.txt")
                if os.path.exists(output_bin_path) and os.path.exists(output_txt_path):
                    print(f"Skipping encoding for {video_name}_M{preset}_CRF{crf} as output already exists.")
                    continue
                encoding_command_line = f"/usr/bin/time --verbose {enc_ffmpeg_path} -y -i {original_video_path} -crf {crf} -preset {preset} -c:v libsvtav1 -f ivf {output_bin_path} > {output_txt_path} 2>&1"
                print(f"Running: {encoding_command_line}")
                run_command(encoding_command_line)

# Function to decode videos and measure quality metrics
def decode_videos(videos, dec_ffmpeg_path, encoded_directory, decoded_directory, original_videos_directory, presets, crfs):
    os.makedirs(decoded_directory, exist_ok=True)
    for video in videos:
        video_name = os.path.splitext(video)[0]
        original_video_path = os.path.join(original_videos_directory, video)
        for preset in presets:
            for crf in crfs:
                input_bin_path = os.path.join(os.path.abspath(encoded_directory), f"{video_name}_M{preset}_CRF{crf}.bin")
                output_xml_path = os.path.join(os.path.abspath(decoded_directory), f"{video_name}_M{preset}_CRF{crf}.xml")
                output_log_path = os.path.join(os.path.abspath(decoded_directory), f"{video_name}_M{preset}_CRF{crf}.log")
                if os.path.exists(output_xml_path) and os.path.exists(output_log_path):
                    print(f"Skipping decoding for {video_name}_M{preset}_CRF{crf} as output already exists.")
                    continue
                decoding_command_line = f"{dec_ffmpeg_path} -y -nostdin -r 25 -i {input_bin_path} -r 25 -i {original_video_path} -lavfi '[0:v][1:v]libvmaf=aom_ctc=1:log_path={output_xml_path}:log_fmt=xml' -f null - > {output_log_path} 2>&1"
                print(f"Running: {decoding_command_line}")
                run_command(decoding_command_line)

# Function to parse txt file
def parse_txt_file(txt_file):
    with open(txt_file, 'r') as file:
        content = file.read()
    bitrate_matches = re.findall(r'bitrate\s*=\s*(\d+(\.\d+)?)kbits/s', content)
    bitrate = bitrate_matches[-1][0] if bitrate_matches else 'N/A'
    
    user_time_match = re.search(r'User time \(seconds\): (\d+\.\d+)', content)
    user_time = user_time_match.group(1) if user_time_match else 'N/A'
    
    sys_time_match = re.search(r'System time \(seconds\): (\d+\.\d+)', content)
    sys_time = sys_time_match.group(1) if sys_time_match else 'N/A'
    
    max_memory_match = re.search(r'Maximum resident set size \(kbytes\): (\d+)', content)
    max_memory = max_memory_match.group(1) if max_memory_match else 'N/A'
    
    file_size_match = re.search(r'Lsize=\s*(\d+)', content)
    file_size = file_size_match.group(1) if file_size_match else 'N/A'
    
    return bitrate, file_size, user_time, sys_time, max_memory

# Function to parse xml file
def parse_xml_file(xml_file):
    tree = ET.parse(xml_file)
    root = tree.getroot()
    psnr_values = []
    ssim_values = []
    vmaf_values = []
    for frame in root.findall(".//frame"):
        psnr_values.append(float(frame.attrib['psnr_y']))
        ssim_values.append(float(frame.attrib['float_ssim']))
        vmaf_values.append(float(frame.attrib['vmaf']))
    harmonic_mean_psnr = len(psnr_values) / sum([1 / v for v in psnr_values]) if psnr_values else 'N/A'
    harmonic_mean_ssim = len(ssim_values) / sum([1 / v for v in ssim_values]) if ssim_values else 'N/A'
    harmonic_mean_vmaf = len(vmaf_values) / sum([1 / v for v in vmaf_values]) if vmaf_values else 'N/A'
    return harmonic_mean_psnr, harmonic_mean_ssim, harmonic_mean_vmaf

# Function to extract data
def extract_data(clip_name, resolution, fps, preset, crf, txt_file, xml_file):
    bitrate, file_size, user_time, sys_time, max_memory = parse_txt_file(txt_file)
    psnr, ssim, vmaf = parse_xml_file(xml_file)
    return [
        clip_name, resolution, fps, preset, crf, bitrate, file_size, user_time, sys_time, max_memory, psnr, ssim, vmaf
    ]


# In the Main function, User will enter all his needed parameters to encode, decode & generate a text file with detailed metrics for his choosen videos  
# Please follow the comments below (between the two ------ lines) to get informed about all placeholders need to be filled out 
def main(args,arg_count):

# -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
    # you need to set the Paths:
    enc_ffmpeg_path = os.path.abspath(args.enc_path)                            # please provide the absolute path to the ffmpeg executable used for encoding (not the same with the current directory)
    dec_ffmpeg_path = os.path.abspath(args.dec_path)                                      # please provide the absolute path to the ffmpeg executable used for decoding (not the same with the current directory)
    original_videos_directory = os.path.abspath(args.original_dir)                     # pleasae provide the directory containing the original .y4m video file
    encoded_directory = os.path.abspath(args.encoded_dir)                # Note: Assuming the user is working on the same root path as the script's directory (and the current directory), please fill out the folder name
    decoded_directory = os.path.abspath(args.decoded_dir)                # Note: Assuming the user is working on the same root path as the script's directory (and the current directory), please fill out the folder name
    
    # you need to set Presets, CRFs and videos you want to process:
    presets = args.presets                       # please input the presets intended to be applied to the video, separated by commas ","
    crfs = args.crfs                    # please input the CRFs intended to be applied to the video, separated by commas ","
    
    # Note: Please place all the videos you want to encode in this directory "original_videos_directory"
    # Get list of all existed videos in the "original_videos_directory"
    videos = os.listdir(original_videos_directory)

    # Check if there are non-.y4m files in the directory
    for video in videos:
        if not video.endswith('.y4m'):
            raise ValueError(f"File {video} is not a .y4m file")
    
    # Set headers, resolution & fps
    headers = [
        'clip_name', 'resolution', 'fps', 'preset', 'CRF', 'bitrate (kbps)', 'file_size', 'user_time', 
        'sys_time', 'max_memory', 'PSNR', 'SSIM', 'VMAF'
    ]
    resolution = args.resolution                    # Please input the resolution as a "string"
    fps = args.fps                    # Please input the frames per second (fps) as an "integer"

    # please input the relative path of where you which to find the txt file that contains all the inforamation about the videos between the ' '                  
    output_csv_path = os.path.abspath(args.output_file)   # Note: don't forget to add the extension '.txt'

# -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    # Encode the videos
    encode_videos(videos, enc_ffmpeg_path, original_videos_directory, encoded_directory, presets, crfs)
    # Decode the videos & measure quality metrics
    decode_videos(videos, dec_ffmpeg_path, encoded_directory, decoded_directory, original_videos_directory, presets, crfs)


    data = []
    for video in videos:
        clip_name = os.path.splitext(video)[0]
        for crf in crfs: 
            for preset in presets:
                txt_file = os.path.join(encoded_directory, f'{clip_name}_M{preset}_CRF{crf}.txt')
                xml_file = os.path.join(decoded_directory, f'{clip_name}_M{preset}_CRF{crf}.xml')
                if os.path.exists(txt_file) and os.path.exists(xml_file):
                    data.append(extract_data(clip_name, resolution, fps, preset, crf, txt_file, xml_file))

    with open(output_csv_path, 'w', newline='') as txtfile:
        writer = csv.writer(txtfile, delimiter='\t') 
        writer.writerow(headers)
        writer.writerows(data)


if __name__ == "__main__":
    args = parser.parse_args()
    arg_count = len(sys.argv) # The number of arguments including the script name. It is passed to the main function in case needed. 
    main(args,arg_count)
