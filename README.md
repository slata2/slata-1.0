## Slata-1.0

Slata-1.0 is a project that consists of scripts that perform encoding, decoding, and information extraction.
After building any ffmpeg version using any SVT-AV1 version, slata-1.0 will allow you to perform encoding,
decoding, and extract video-info and metric values (PSNR,SSIM,VMAF "harmonic mean") using SVT-AV1 Encoder
and the user's intended crfs and presets values on y4m videos.

The canonical URL for this project is at https://gitlab.com/slata2/slata-1.0

## Linux* Operating Systems (64-bit)


## Installation
- Please follow the following steps to be able to use slata-1.0 :
- __1. Build and Install SVT-AV1__
``` bash
git clone https://gitlab.com/AOMediaCodec/SVT-AV1.git
cd SVT-AV1
git tag (optional)
git checkout <input v and your intended SVT-AV1 version> (optional)
cd Build
cmake .. -G"Unix Makefiles" -DCMAKE_BUILD_TYPE=Release
make -j $(nproc)
sudo make install
```
- __2. Checking out to a specific libsvt-av1: (optional)__
``` bash 
git checkout <commit_hash>
``` 
- __3. Build, Install, and enable libsvt-av1 in ffmpeg:__
``` bash
git clone --depth=1 https://github.com/FFmpeg/FFmpeg ffmpeg
cd ffmpeg
git tag (optional)
git checkout <input n and your intended ffmpeg version> (optional)
export LD_LIBRARY_PATH+=":/usr/local/lib"
export PKG_CONFIG_PATH+=":/usr/local/lib/pkgconfig"
./configure --enable-libsvtav1
make -j $(nproc)
```
- __4. Clone and use slata-1.0 scripts:__
- anything between " " is an example of what you should input.
``` bash
git clone https://gitlab.com/slata2/slata-1.0
cd slata-1.0
sudo apt update
sudo apt install python3
python3 avocado.py --videos "video1.y4m video2.y4m" --presets "prset1 preset2" --crfs "crf1 crf2" --fps "fps1 fps2" --resolution "1920x1080" --enc_path " 'SVT-AV1/Build/ffmpeg/ffmpeg' " --dec_path " 'SVT-AV1/Build/ffmpeg/ffmpeg_s' " --original_dir "clips" --encoded_dir "en1" --decoded_dir "de1" 
```

## Support
- <benromdhanetaha14@yahoo.fr>
- <ahmedkossentini01@gmail.com>
- <khalilkhemakhem3@gmail.com>
- <triguiiheb@gmail.com>


## Contributing

We welcome contributions to this project! If you would like to contribute, please follow these steps:

1. Fork the repository on GitLab.
2. Clone your forked repository to your local machine:
   ```bash
   git clone https://gitlab.com/your-username/slata-1.0.git
   cd slata-1.0

Let one of us know via email that you are planning to contribute:

- Hassen Tmar: <hassenetmar@gmail.com>
- Taha BenRomdan: <benromdhanetaha14@yahoo.fr>
- Mohamed Iheb Trigui: <triguiiheb@gmail.com>
- Ahmed Kossentini: <ahmedkossentini01@gmail.com>
- Khalil Khmekem: <khalilkhemakhem3@gmail.com>


## Authors and acknowledgment
A big thank you to the following people who have contributed to this project:

- Hassene Tmar
- Taha Ben Romdhane
- Mohamed Iheb Trigui
- Ahmed Kossentini
- Khalil Khemakhem

## License

This project is licensed under the BSD 2-Clause License. See the [LICENSE](https://gitlab.com/slata2/slata-1.0/-/blob/main/LICENSE.md?ref_type=heads) for more information.


## Project status

This project is completed and works as expected. Development is currently on hold, but future updates may be made to enhance functionality or address any issues that arise. Contributions and suggestions are welcome.

